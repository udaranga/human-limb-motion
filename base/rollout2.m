%% rollout.m
% *Summary:* Generate a state trajectory using an ODE solver (and any additional
% dynamics) from a particular initial state by applying either a particular
% policy or random actions.
%
%   function [x y L latent] = rollout(start, policy, H, plant, cost)
%
% *Input arguments:*
%
%   start       vector containing initial states (without controls)   [nX  x  1]
%   policy      policy structure
%     .fcn        policy function
%     .p          parameter structure (if empty: use random actions)
%     .maxU       vector of control input saturation values           [nU  x  1]
%   H           rollout horizon in steps
%   plant       the dynamical system structure
%     .subplant   (opt) additional discrete-time dynamics
%     .augment    (opt) augment state using a known mapping
%     .constraint (opt) stop rollout if violated
%     .poli       indices for states passed to the policy
%     .dyno       indices for states passed to cost
%     .odei       indices for states passed to the ode solver
%     .subi       (opt) indices for states passed to subplant function
%     .augi       (opt) indices for states passed to augment function
%   cost    cost structure
%
% *Output arguments:*
%
%   x          matrix of observed states                           [H   x nX+nU]
%   y          matrix of corresponding observed successor states   [H   x   nX ]
%   L          cost incurred at each time step                     [ 1  x    H ]
%   latent     matrix of latent states                             [H+1 x   nX ]
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modification: 2013-05-21
%
%% High-Level Steps
%
% # Compute control signal $u$ from state $x$:
% either apply policy or random actions
% # Simulate the true dynamics for one time step using the current pair $(x,u)$
% # Check whether any constraints are violated (stop if true)
% # Apply random noise to the successor state
% # Compute cost (optional)
% # Repeat until end of horizon



function [x y L latent] = rollout2(start, policy, H, plant, cost)
%% Code
global initialized;

initialized = 0;
if isfield(plant,'augment'), augi = plant.augi;             % sort out indices!
else plant.augment = inline('[]'); augi = []; end
if isfield(plant,'subplant'), subi = plant.subi;
else plant.subplant = inline('[]',1); subi = []; end
odei = plant.odei; poli = plant.poli; dyno = plant.dyno; angi = plant.angi;
simi = sort([odei subi]);
nX = length(simi)+length(augi); nU = length(policy.maxU); nA = length(angi);

state(simi) = start; state(augi) = plant.augment(state);      % initializations
x = zeros(H+1, nX+2*nA);
x(1,simi) = start' + randn(size(simi))*chol(plant.noise);
x(1,augi) = plant.augment(x(1,:));
u = zeros(H, nU); latent = zeros(H+1, size(state,2)+nU);
y = zeros(H, nX); L = zeros(1, H); next = zeros(1,length(simi));

for i = 1:H % --------------------------------------------- generate trajectory
    s = x(i,dyno)'; sa = gTrig(s, zeros(length(s)), angi); s = [s; sa];
    x(i,end-2*nA+1:end) = s(end-2*nA+1:end);
    
    % 1. Apply policy ... or random actions --------------------------------------
    if isfield(policy, 'fcn')
        u(i,:) = policy.fcn(policy,s(poli),zeros(length(poli)));
    else
        u(i,:) = policy.maxU.*(2*rand(1,nU)-1);
        %     u(i,:) = policy.maxU.*rand(1,nU);
    end
    latent(i,:) = [state u(i,:)];                                  % latent state
    
    % 2. Simulate dynamics -------------------------------------------------------
    limitsU = policy.maxU;
    S_flexLimits = [0.01 1];
    S_extLimits = [0.01 1];
    S_flex = S_flexLimits(1) + (u(i,1)+limitsU(1))*(S_flexLimits(2)-S_flexLimits(1))/(2*limitsU(1));
    S_ext  = S_extLimits(1) + (u(i,2)+limitsU(2))*(S_extLimits(2)-S_extLimits(1))/(2*limitsU(2));
    tempU = [S_flex S_ext];
    
%     S_flex_All = [0.002498, 0.009967, 0.022332, 0.039470, 0.061209, 0.087332, 0.117579, 0.151647, 0.189195, 0.229849, 0.273202, 0.318821, 0.366251, 0.415016, 0.464631, 0.514600, 0.564422, 0.613601, 0.661645, 0.708073, 0.752423, 0.794251, 0.833138, 0.868697, 0.900572, 0.928444, 0.952036, 0.971111, 0.985479, 0.994996, 0.999568, 0.999873, 0.999494, 0.998863, 0.997980, 0.996849, 0.995471, 0.993850, 0.991987, 0.989888, 0.987556, 0.984996, 0.982214, 0.979214, 0.976003, 0.972588, 0.968976, 0.965173, 0.961187, 0.957027, 0.952701, 0.948218, 0.943586, 0.938816, 0.933916, 0.928897, 0.923769, 0.918542, 0.913227, 0.907835, 0.902376, 0.896862, 0.891303, 0.885711, 0.880098, 0.874475, 0.868852, 0.863242, 0.857656, 0.852105, 0.846600, 0.841152, 0.835774, 0.830474, 0.825265, 0.820157, 0.815159, 0.810283, 0.805538, 0.800933, 0.796478, 0.792183, 0.788055, 0.784103, 0.780335, 0.776758, 0.773381, 0.770209, 0.767250, 0.764509, 0.761991, 0.759702, 0.757647, 0.755829, 0.754253, 0.752921, 0.751836, 0.751000, 0.750416, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000, 0.750000];
%     S_ext_All = [0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000, 0.010000];
%     tempU = [S_flex_All(i) S_ext_All(i)];
    
    next(odei) = simulate2(state(odei), tempU, plant);
    next(subi) = plant.subplant(state, u(i,:));
%       fprintf('%f %f %f %f matlAB\n',tempU(1),tempU(2),state(2)*180/pi,next(2)*180/pi);
    
    % 3. Stop rollout if constraints violated ------------------------------------
    if isfield(plant,'constraint') && plant.constraint(next(odei))
        H = i-1;
        fprintf('state constraints violated...\n');
        break;
    end
    
    % 4. Augment state and randomize ---------------------------------------------
    state(simi) = next(simi); state(augi) = plant.augment(state);
    x(i+1,simi) = state(simi) + randn(size(simi))*chol(plant.noise);
    x(i+1,augi) = plant.augment(x(i+1,:));
    
    % 5. Compute Cost ------------------------------------------------------------
    if nargout > 2
        cost.tstep = i;
        L(i) = cost.fcn(cost,state(dyno)',zeros(length(dyno)));
        %     cost = rmfield(cost, 'tstep');
    end
end

y = x(2:H+1,1:nX); x = [x(1:H,:) u(1:H,:)];
latent(H+1, 1:nX) = state; latent = latent(1:H+1,:); L = L(1,1:H);