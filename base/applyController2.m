%% applyController.m
% *Summary:* Script to apply the learned controller to a (simulated) system
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-06-04
%
%% High-Level Steps
% # Generate a single trajectory rollout by applying the controller
% # Generate many rollouts for testing the performance of the controller
% # Save the data

%% Code

% 1. Generate trajectory rollout given the current policy
if isfield(plant,'constraint'), HH = maxH; else HH = H; end
[xx, yy, realCost{j+J}, latent{j}] = ...
    rollout2(gaussian(mu0, S0), policy, HH, plant, cost);

disp(xx);                           % display states of observed trajectory
x = [x; xx]; y = [y; yy];                            % augment training set
if plotting.verbosity > 0
    if ~ishandle(3); figure(3); else set(0,'CurrentFigure',3); end
    hold on; plot(1:length(realCost{J+j}),realCost{J+j},'r'); drawnow;
end 

% 2. Make many rollouts to test the controller quality
if plotting.verbosity > 1
    lat = cell(1,10);
    for i=1:10
        [~,~,~,lat{i}] = rollout2(gaussian(mu0, S0), policy, HH, plant, cost);
    end
    
    if ~ishandle(4); figure(4); else set(0,'CurrentFigure',4); end; clf(4);
    
    ldyno = length(dyno);
    for i=1:ldyno       % plot the rollouts on top of predicted error bars
        
        % Plot model prediction
        subplot(2,2,i); hold on;   
        if i == 1
           errorbar( 0:length(M{j}(i,:))-1, M{j}(i,:), ...
            2*sqrt(squeeze(Sigma{j}(i,i,:))) );
        elseif i == 2
          
            errorbar( 0:length(M{j}(i,:))-1, (M{j}(i,:)- cost.target(2,:))*180/pi, ...
            2*sqrt(squeeze(Sigma{j}(i,i,:))) );
            
        end
        
        % Plot 10 rollout results
        for ii=1:10
             
            if i == 1; yyy = lat{ii}(:,dyno(i));
            elseif i == 2;  yyy = (lat{ii}(:,dyno(i)) - cost.target(2,:)')*180/pi;
            end
            plot( 0:size(lat{ii}(:,dyno(i)),1)-1, yyy , 'r' );
            
        end
        
        % Plot the original rallout (results obtained for the GP model
        % training)
         
        if i == 1;  yyy = latent{j}(:,dyno(i));
        elseif i == 2;  yyy = (latent{j}(:,dyno(i)) - cost.target(2,:)')*180/pi;
        end
        plot( 0:size(latent{j}(:,dyno(i)),1)-1, yyy ,'g');
        axis tight
%         cost = rmfield(cost, 'target');
        
        if i == 2; ylabel('\theta - \theta_{target}');
        elseif i == 1; ylabel('Angular Speed');
        end
        xlabel('Time Step');
    end
    drawnow;
    
    % Compute S_flex and S_ext
    for ii=1:10
        uu = lat{ii}(:,3:4);
        limitsU = policy.maxU;
        S_flexLimits = [0.01 1];
        S_extLimits = [0.01 1];
        S_flex(:,ii) = S_flexLimits(1) + (uu(:,1)+limitsU(1))*(S_flexLimits(2)-S_flexLimits(1))/(2*limitsU(1));
        S_ext(:,ii)  = S_extLimits(1) + (uu(:,2)+limitsU(2))*(S_extLimits(2)-S_extLimits(1))/(2*limitsU(2));
        tempU = [S_flex S_ext];
        
    end
    
     % Plot S_flex
    subplot(2,2,3); hold on;
    errorbar( 0:size(lat{ii}(:,dyno(i)),1)-1, mean(S_flex,2), ...
        2*std(S_flex,0,2) );
    HHH = size(lat{ii}(:,dyno(i)),1);
    plot( 0:size(lat{ii}(:,dyno(i)),1)-1, 21*ones( HHH,1),'--g');
    plot( 0:size(lat{ii}(:,dyno(i)),1)-1, 5*ones( HHH,1),'--g');
    ylabel('S_{flex}');
    xlabel('Time Step');
    axis tight
    
    % Plot S_ext
    subplot(2,2,4); hold on;
    errorbar( 0:size(lat{ii}(:,dyno(i)),1)-1, mean(S_ext,2), ...
        2*std(S_ext,0,2) );
    HHH = size(lat{ii}(:,dyno(i)),1);
    plot( 0:size(lat{ii}(:,dyno(i)),1)-1, 5*ones( HHH,1),'--g');
    plot( 0:size(lat{ii}(:,dyno(i)),1)-1, 0.5*ones( HHH,1),'--g');
    ylabel('S_{ext}');
    xlabel('Time Step');
    axis tight
    
    %     uu = latent{j}(:,3:4);
    %     limitsU = policy.maxU;
    %     S_flexLimits = [5 21];
    %     S_extLimits = [0.5 5];
    %     S_flex = S_flexLimits(1) + (uu(:,1)+limitsU(1))*(S_flexLimits(2)-S_flexLimits(1))/(2*limitsU(1));
    %     S_ext  = S_extLimits(1) + (uu(:,2)+limitsU(2))*(S_extLimits(2)-S_extLimits(1))/(2*limitsU(2));
    %     tempU = [S_flex S_ext];
    %
    %
    %
    %     subplot(2,2,3); hold on;
    %     plot(0:size(latent{j}(:,dyno(i)),1)-1, S_flex ,'g');
    %     ylabel('S_{flex}');
    %     xlabel('Time Step');
    %     axis tight
    %
    %     subplot(2,2,4); hold on;
    %     plot( 0:size(latent{j}(:,dyno(i)),1)-1, S_ext ,'g');
    %     ylabel('S_{ext}');
    %     xlabel('Time Step');
    %
    
    
    
end


% 3. Save data
filename = [basename num2str(j) '_H' num2str(H)]; save(filename);
