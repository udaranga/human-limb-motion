#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 11:45:22 2014

Hill Type Muscle Model

@author: Berat Denizdurduran, @email: berat.denizdurduran@epfl.ch
"""

import numpy as np
import scipy.integrate as integrate


class HillMuscle(object):
    """
    theta is in radial form and it is coming with state[0] of arm controller
    """
    
    init_sta = [0.0] # A, 
    init_sta = np.asarray(init_sta, dtype = 'float')
    
    #tau = 10.00 # 0.01s
    tau = 0.01
    
    delta_lmtu = 0.0
    angle_TR = 0.0
     
    l_slack = 0.13
    l_opt = 0.11
    l_ce = 0.0
    l_se = 0.0
    
    r_0 = 0.1
    penn = 0.5
    
    w = 0.56
    E_ref = 0.04
    
    r0 = 0.2
    l0 = 0.8
    
    c = np.log(0.05)
    
    F_max = 15.0
    v_max = 1.2
    
    K = 5.0
    N = 1.5 
    
    lmtu = l_slack + l_opt + delta_lmtu
    
    #theta_max = np.sin(np.deg2rad(180))
    #theta_max = np.deg2rad(135.0)
    #theta_ref = np.deg2rad(100.0)    
    
    
    def __init__(self, ref_theta):
        
        super(HillMuscle, self).__init__()
        self.theta_ref = ref_theta
        self.hill_muscle()        
        
        
    def hill_muscle(self):
        
        self.state = self.init_sta
        #self.plot.append(self.state)
        self.time_passed = 0
       
       
    def init_muscle_length(self, degree):
        
        #theta_func, new_lmtu = self.muscle_comp_lengths(degree)
        
        theta_func, new_lmtu = self.angleTransfer(degree)
        
        if new_lmtu < self.lmtu:
            self.l_ce = self.l_opt
            self.l_se = new_lmtu - self.l_ce 
            #print("initial position is slack")
        else:
            self.l_se = self.l_slack * ((self.l_opt * self.w + self.E_ref * (new_lmtu - self.l_opt))/(self.l_opt * self.w + self.E_ref * self.l_slack))
            self.l_ce = new_lmtu - self.l_se
            #print("initial position is not slack")
            
        #print("l_ce = %f" % self.l_ce)
        #print("l_se_init = %f" % self.l_se)
        #print("l_mtu = %f" % new_lmtu)
        
        
    def angleTransfer(self, angle):
        
        #self.angle_TR = np.sin(angle - self.theta_max)-np.sin(self.theta_ref - self.theta_max)
        
        self.angle_TR = angle - self.theta_ref
        self.delta_lmtu = self.r_0*self.penn*self.angle_TR
        lmtu = self.l_slack + self.l_opt + self.delta_lmtu
        
        return self.angle_TR, lmtu
        
    
    def muscle_comp_lengths(self, angle):
        """
        The first idea of the muscle length calculation
        NOT USED
        """
        numerator = self.r0*self.l0*np.sin(angle)
        denominator = np.sqrt( self.r0**2 + self.l0**2 - 2*self.r0*self.l0*np.cos(angle) ) # this is equal to lmtu
        
        return numerator / denominator, denominator - 0.58  # -0.58 in order to match with the reference


    def compute_f_se(self, l_se):
        
        epsilon = ( l_se - self.l_slack) / self.l_slack
        #print("epsilon = %f" % epsilon)
        
        if epsilon > 0.0:
            F_se = self.F_max * (epsilon / self.E_ref)**2
        else:
            F_se = 0.0
            
        return F_se
    
    
    def compute_f_be(self, l_se, l_ce):
        
        
        numerator = (l_ce-self.l_opt*(1.0-self.w)) 
        
        denominator = (self.l_opt * self.w)/2.0 
        
        if l_ce <= self.l_opt*(1-self.w): 
            F_be = self.F_max * ( numerator / denominator )**2 
        else: 
            F_be = 0.0

        return F_be

    
    def compute_f_pe_s(self, l_ce):
        
        numerator = l_ce - self.l_opt
        denominator = self.l_opt * self.w
    
        if l_ce > self.l_opt:
            F_pe_s = self.F_max * ( numerator / denominator )**2
        else: 
            F_pe_s = 0.0
            
        return F_pe_s
        
    
    def compute_fl(self, lce):
                
        numerator = lce - self.l_opt
        denominator = self.l_opt * self.w
        
        val = np.abs( numerator / denominator )
        exposant = self.c * (val ** 3)
        
        return np.exp(exposant)
        
        
    def compute_fv(self, fse, fbe, fpe_s, A, fl):
        
        nominator = fse + fbe
        denominator = self.F_max * A * fl + fpe_s
        
        if float(denominator) == 0.0:
            fv = 0.0
        else:
            fv = nominator / denominator
                
        if fv > 1.5:
            fv = 1.5
                
        if fv < 0.0:
            fv = 0.0
                
        return fv
        
        
    def compute_vce(self, fv):
        
        if fv <= 1.0:
            nominator = self.v_max * self.l_opt * ((1 - fv)/(1+self.K * fv))
        else:
            nominator = self.v_max * self.l_opt * ((fv - 1 )/ (7.56 * self.K * (fv - self.N) + 1 - self.N))
        
        return nominator
        
    
    def activity(self, state, t, S):
        
        der1 = (S-state)/self.tau
        
        return der1
        
    
    def length_feedback(self, l_ce, l_offset):
        
        return (l_ce / self.l_opt) - l_offset
        
    
    def force_feedback(self, f_mtu):
        
        return f_mtu / self.F_max
        
        
    def iteration(self, dt, degree, S):
                
        if self.time_passed == 0.:
            self.init_muscle_length(degree)
                       
        self.state = integrate.odeint(self.activity, self.state, [0, dt], args=(S,))[1]
        
        F_se = self.compute_f_se(self.l_se)

        F_be = self.compute_f_be(self.l_se, self.l_ce)

        F_pe_s = self.compute_f_pe_s(self.l_ce)

        fl = self.compute_fl(self.l_ce)

        fv = self.compute_fv(F_se, F_be, F_pe_s, self.state, fl)        
        
        F_ce = F_se - F_pe_s*fv + F_be 

        vce = self.compute_vce(fv)
        
        self.l_ce  = self.l_ce - vce*dt
        
        theta_func, lmtu = self.angleTransfer(degree)
        self.l_se = lmtu - self.l_ce
        
        self.time_passed += dt
        
        theta_func = 1.
        
        return theta_func, F_se