function next = simulate2(x0, f, plant)

% In PILCO
% x0         = omega1, omega2, theta1, theta2
% In muscle model - python
% state      = theta1, omega1, theta2, omega2
% stateShort = theta1, omega1,

% f - action (u1,u2)

addpath('..\..\pythonInMatlab');

py('set', 'x0', eval('x0'));
py('set', 'f', eval('f'));

str = sprintf('execfile(''hillArmControl.py'')');
py('eval', str);

nextTemp = py('get', 'nextState');

next = [ 0, nextTemp(2), pi ,nextTemp(1)];

end