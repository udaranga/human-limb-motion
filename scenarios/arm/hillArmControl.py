#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 24 16:32:29 2014

An Arm controlled by Hill-Type muscle model

@author: Berat Denizdurduran, @email: berat.denizdurduran@epfl.ch
"""

import numpy as np
import scipy.integrate as integrate
  
import hillMuscleModel

pause = False


class SimpleArm(object):
    
    L = np.ones(1) # l1, l2 = 1 m
    M = np.ones(1) # m1, m2 = 1 kg
    I = np.ones(1) # i1, i2 = 0
    G = 9.81 # gravity
    
    l0 = L*0.8
    r0 = L*0.2
    
    b = 0.6 # damping coefficient
    
    joint_angle = 0.0
    count = 1
    
    position_mat = np.zeros((2,2))
    
    init_sta = [np.deg2rad(90), 0] # states are defined between -pi and pi
    init_sta = np.asarray(init_sta, dtype = 'float')
    
    
    def __init__(self):
        
        super(SimpleArm, self).__init__()
        self.state = self.init_sta
        self.time_passed = 0
        self.muscle_ext = hillMuscleModel.HillMuscle(np.deg2rad(100.0)) 
        self.muscle_flex = hillMuscleModel.HillMuscle(np.deg2rad(100.0))
        # state = theta1, omega1, theta2, omega2
        
        
    def position(self):
        
        x1 = self.L[0]*np.sin(self.state[0])
        y1 = -self.L[0]*np.cos(self.state[0])
        
        self.position_mat[0][1],  self.position_mat[1][1] = x1, y1 
        self.position_mat = np.cumsum(self.position_mat, axis=1)
     
        return (self.position_mat[0], self.position_mat[1])
                       
    
    def euler_lagrangian(self, state, t ,u):
        
        #u = 0
        der = np.zeros_like(state)
        
        der1 = state[1]
        der2 = (u-(self.M[0]*self.G*self.L[0]*np.sin(state[0])+self.b*der1) / (self.M[0]*self.L[0]*self.L[0])) # b*theta' for damping
        #der2 = (u-(self.M[0]*self.G*self.L[0]*np.sin(state[0])) / (self.M[0]*self.L[0]*self.L[0])) # b*theta' for damping
        #print(der2)

        der[0], der[1] = der1, der2
        return der
        
        
    def iteration(self, dt, x0, f):
        
        # PILCO format ->
        # x0    = omega1, omega2, theta1, theta2
        # muscle model format ->
        # state = theta1, omega1, theta2, omega2
 
        self.state[0], self.state[1]  = x0[0][1], x0[0][0]
        
        self.joint_angle = np.deg2rad(180) - self.state[0]
    
#        S_ext = 0.1
#        S_flex = 17.7
        
        S_flex =   f[0][0] 
        S_ext  =   f[0][1]
        
        
#        print  "%d %f %f %f" % (self.count,S_flex,S_ext, np.rad2deg(self.state[0])) 
        self.count = self.count+1
        
        theta_func_ext, F_se_ext = self.muscle_ext.iteration(dt, self.joint_angle, S_ext)
        theta_func_flex, F_se_flex = self.muscle_flex.iteration(dt, self.joint_angle, S_flex)
		
        
        force_ext = F_se_ext * theta_func_ext 
        force_flex = F_se_flex * theta_func_flex 
               
        #statis_force = 9.81
        statis_force = 0.0
        u = statis_force - force_ext*0.1 + force_flex*0.1;
        
        
#        print '%f '  %(self.muscle_ext.F_max)         
        
        
        self.state = integrate.odeint(self.euler_lagrangian, self.state, [0, dt], args=(u,))[1]
        
        self.time_passed += dt
        
        nextState = self.state
        
#        print "%f %f" %(S_flex,np.rad2deg(nextState[0]))

        return self.time_passed, self.joint_angle, nextState
   
 
def animate(x0,f):
    
    global muscle, dt
    
    if not pause:
        
        y, joint_angle, nextState  = muscle.iteration(dt,x0,f)

        
        position = muscle.position()
        
      
        
        position_muscle_ext[0][0], position_muscle_ext[0][1] = 0.0, position[0][1]*0.2
        position_muscle_ext[1][0], position_muscle_ext[1][1] = 0.8, position[1][1]*0.2
        
        position_muscle_flex[0][0], position_muscle_flex[0][1] = 0.0, -position[0][1]*0.2
        position_muscle_flex[1][0], position_muscle_flex[1][1] = 0.8, -position[1][1]*0.2
        
         
    return nextState
   
def onClick(event):
    
    global pause
    
    pause ^= True
    # use this part to plot the changes


if initialized == 0:
    
    initialized = 1;
    muscle  = SimpleArm()
    dt = 0.01
    
    position_muscle_ext = np.zeros((2,2))
    position_muscle_flex = np.zeros((2,2))
    
     
     
     
    global line, line2, muscle_point_ext , muscle_point_flex

 
 
#while 1:
nextState = animate(x0,f)

#    time.sleep(0.1)  

# why there is an i in animate function ?
# omega is dtheta, isn't it ?
# what is the "initial position is a slack/not slack"
# range of S_ext and S_flex
