%% draw_rollout_pendulum.m
% *Summary:* Script to draw a trajectory of the most recent pendulum trajectory
%
% Copyright (C) 2008-2013 by
% Marc Deisenroth, Andrew McHutchon, Joe Hall, and Carl Edward Rasmussen.
%
% Last modified: 2013-03-27
%
%% High-Level Steps
% # For each time step, plot the observed trajectory

%% Code
% path = 'C:\Users\Udaranga\OneDrive\EPFL - Masters\Semester 2\CS 498 Semester Project\documentation';
% file = '\target55-60_with_damping_sc_8_17';
% file = '\target55-55-60_step_with_damping_sc_8(4)_14';
% writerObj = VideoWriter([path,file,'.avi']);
% writerObj.FrameRate = 20;
% open(writerObj);

 
% Loop over states in trajectory
for r = 1:size(xx,1)
    cost.t = r;
    cost.tstep = r;
    if exist('j','var') && ~isempty(M{j})
        draw_pendulum(latent{j}(r,2), latent{j}(r,end), cost,  ...
            ['trial # ' num2str(j+J) ', T=' num2str(H*dt) ' sec'], ...
            ['total experience (after this trial): ' num2str(dt*size(x,1)) ...
            ' sec'], M{j}(:,r), Sigma{j}(:,:,r));
    else
        draw_pendulum(latent{jj}(r,2), latent{jj}(r,end), cost,  ...
            ['(random) trial # ' num2str(1) ', T=' num2str(H*dt) ' sec'], ...
            ['total experience (after this trial): ' num2str(dt*size(x,1)) ...
            ' sec']);
    end
    pause(dt);
    %   cost = rmfield(cost, 'tstep');
    
    
%     frame = getframe;
%     writeVideo(writerObj,frame);
    
%     angles(r) = latent{j}(r,2);
end
% close(writerObj);
% 
% 
% figure;
% %
% t = dt:dt:H*dt;
% plot(t,angles*180/pi);hold on;
% plot(t,cost.target(2,1:H)*180/pi,'--');
% grid on;
% ylabel('Angle \theta');
% xlabel('Time (s)');
% set(gcf,'PaperPositionMode','auto')
% print([path,file],'-dpng','-r0')
