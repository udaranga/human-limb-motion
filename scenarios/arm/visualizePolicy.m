clear all;
load('run7_multipleRuns\pendulum_19_H80.mat');

thetaDot = reshape(x(:,1),80,20);
theta = reshape(x(:,2),80,20)*180/pi;
u1 = reshape(x(:,5),80,20);
u2 = reshape(x(:,6),80,20);

%%
h = 1:H;
for i = 1:20
    if i < 15
        plot3(h,theta(:,i),thetaDot(:,i),'-.');hold on;
    else
        plot3(h,theta(:,i),thetaDot(:,i),'-*');hold on;
    end
end
grid on;
xlabel('Time Step');
ylabel('Angle');
zlabel('Speed');

%%

h = 1:H;
for i = 20:20
    if i < 15
        plot3(h,u1(:,i),u2(:,i),'-.');hold on;
    else
        plot3(h,u1(:,i),u2(:,i),'-*');hold on;
    end
end
grid on;
xlabel('Time Step');
ylabel('u1');
zlabel('u2');


%%
i = 20;
% quiver3(h',theta(:,i),thetaDot(:,i),zeros(80,1),u1(:,i),u2(:,i));
% quiver(h',theta(:,i),zeros(80,1),u1(:,i));hold on;
% quiver(h',theta(:,i),zeros(80,1),u2(:,i));hold on;
plot(h,u1(:,i)+20);hold on;
plot(h,20*u2(:,i)-30);
plot(h,theta(:,i));
xlabel('Time Step');
ylabel('Angle');
legend('S_{flex}','S_{ext}');
grid on;

%%
figure;
quiver(h',theta(:,i),zeros(80,1),u2(:,i));hold on;
plot(h,theta(:,i),'-.');
xlabel('Time Step');
ylabel('Speed/S_{ext}');
 
