
  figure(2);   
  legend(legendInfo);
  saveas( gcf, sprintf('%s/functionValue_%d',str,j), 'jpg' );
  figure(3);
  grid on;
  xlim([0 H])
  saveas( gcf, sprintf('%s/cost_%d',str,j), 'jpg' );
  figure(4);
  subplot(221);grid on;  subplot(222);grid on;  subplot(223);grid on;  subplot(224);grid on;
  saveas( gcf, sprintf('%s/state_%d',str,j), 'jpg' );
  figure(5);
  % Predictions
  errorbar( 0:length(M{j}(2,:))-1, (M{j}(2,:))*180/pi, ...
      2*sqrt(squeeze(Sigma{j}(2,2,:))) );hold on;
  % 10 Rollouts
  for ii=1:10
      yyy = (lat{ii}(:,dyno(2)))*180/pi;
      plot( 0:size(lat{ii}(:,dyno(2)),1)-1, yyy , 'r' );
  end
  % Rollout used for dynmodel
  yyy = (latent{j}(:,dyno(2)))*180/pi;  
  plot( 0:size(latent{j}(:,dyno(2)),1)-1, yyy ,'g');
  % Cost
  costTarget = cost.target*180/pi;
  plot(costTarget(2,:),'m');
  grid on;
  ylabel('Angle (deg)');
  xlabel('Time Step');
  xlim([0 H]);
  saveas( gcf, sprintf('%s/AnglePlot_%d',str,j), 'jpg' );hold off;