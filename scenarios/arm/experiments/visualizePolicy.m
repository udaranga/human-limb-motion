% clear all;
load('C:\Users\Udaranga\OneDrive\EPFL - Masters\Semester 2\CS 498 Semester Project\PILCO\pilcoV0.9\scenarios\arm\run7_multipleRuns\pendulum_1_H80.mat')
thetaDot = -1:0.05:1;
theta = 0:0.1:pi/2;

M = cell(length(thetaDot),length(theta));
S = cell(length(thetaDot),length(theta));
for i = 1:length(thetaDot)
    for j = 1:length(theta)
%         mm = [thetaDot(i),theta(j),sin(theta(j)),cos(theta(j))];
%         ss = eye(4);
        mm = [thetaDot(i);theta(j)];
        ss = 0.5*eye(2);

% %         [M{i,j}, S{i,j}] = policy.fcn(policy, mm(poli)', ss(poli,poli));
        [M{i,j}, S{i,j}] = plant.prop(mm, ss, plant, dynmodel, policy);  
    end
end
%%
% figure(8);
[X, Y] = meshgrid(thetaDot,theta);
X = X(:);
Y = Y(:);
deltaX =zeros(length(X),1);
deltaY =zeros(length(X),1);
for i = 1:length(thetaDot)
    for j = 1:length(theta)
        
        mNext = M{i,j};
        deltaX((i-1)*length(theta)+j) = mNext(1)-X((i-1)*length(theta)+j);
        deltaY((i-1)*length(theta)+j) = mNext(2)-Y((i-1)*length(theta)+j);
        
        
    end
end


X = (X*180/pi);
Y = Y*180/pi;
deltaX = (deltaX*180/pi);
deltaY = deltaY*180/pi;
%%
% hold on;
quiver(X,Y,deltaX,deltaY);

%%
set(gcf,'PaperPositionMode','auto')
print([path,'policy19'],'-dpng','-r0')