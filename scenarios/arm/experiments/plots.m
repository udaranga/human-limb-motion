%%
plot(withoutDamping(1:300,4)*180/pi);hold on;
plot(withDamping2(1:300,4)*180/pi);
plot(withDamping3(1:300,4)*180/pi);
% plot(withDamping4(1:300,4)*180/pi);
plot(withDamping5(1:300,4)*180/pi);
plot(withDamping6(1:300,4)*180/pi);
plot(withDamping7(1:300,4)*180/pi);
plot(withDamping8(1:300,4)*180/pi);
plot(constU(1:300,4)*180/pi);
legend('Without',...
    '1000,1000',...
    '2000,2000',...
    '1200,1000',...
    '1400,1000',...
    '1600,1000',...
    '1500,1000',... %9
    'constU7.5'...
    );grid on;

%%
t = 0:0.05:15-0.05;
plot(t,withoutDamping(1:300,4)*180/pi);hold on;
plot(t,withDamping2(1:300,4)*180/pi);
% plot(withDamping3(1:300,4)*180/pi);
% plot(withDamping4(1:300,4)*180/pi);
% plot(withDamping5(1:300,4)*180/pi);
% plot(withDamping6(1:300,4)*180/pi);
% plot(withDamping7(1:300,4)*180/pi);
plot(t,withDamping8(1:300,4)*180/pi);
% plot(withDamping9(1:300,4)*180/pi);
legend('c_{ext} = 0 ,c_{flex} = 0',...
    'c_{ext} = 1000, c_{flex} = 1000',... %2 
    'c_{ext} = 1500,c_{flex} =1000'... %8
    );grid on;
xlabel('Time (s)');
ylabel('Angle (deg)');

%%
cmp = get(groot,'DefaultAxesColorOrder');
t = 0:0.05:15-0.05;
plot(t,f5_e2(1:300,4)*180/pi,'Color',cmp(1,:));hold on;
plot(t,f5_e2_D(1:300,4)*180/pi,'Color',cmp(2,:)); 
legend('Without Damping','With Damping');

plot(t,f11_e2(1:300,4)*180/pi,'Color',cmp(1,:)); 
plot(t,f11_e2_D(1:300,4)*180/pi,'Color',cmp(2,:)); 
 
plot(t,f20_e2(1:300,4)*180/pi,'Color',cmp(1,:)); 
plot(t,f20_e2_D(1:300,4)*180/pi,'Color',cmp(2,:)); 
 grid on;
%%
plot(t,f5_e5(1:300,4)*180/pi,'Color',cmp(3,:));hold on;
plot(t,f5_e5_D(1:300,4)*180/pi,'Color',cmp(4,:)); 
legend('Without Damping','With Damping');

plot(t,f11_e5(1:300,4)*180/pi,'Color',cmp(3,:)); 
plot(t,f11_e5_D(1:300,4)*180/pi,'Color',cmp(4,:)); 

plot(t,f20_e5(1:300,4)*180/pi,'Color',cmp(3,:)); 
plot(t,f20_e5_D(1:300,4)*180/pi,'Color',cmp(4,:)); 
axis ([0 15 -10 90]);
grid on;

