function next = simulate2(x0, f, plant)

% In PILCO
% x0         = omega1, theta1,    -- theta measured in anti-clockwise dir
% with zero when pendulum is resting

% In muscle model - python
% state      = theta1, omega1, theta2, omega2
% stateShort = theta1, omega1,
% ---- theta1 is measure same as x0 theta1

% f - action (u1,u2)

global initialized;


addpath('..\..\pythonInMatlab');

py('set', 'x0', eval('x0'));
py('set', 'f', eval('f'));
if initialized == 0
    py('set', 'initialized', 0);    
end

str = sprintf('execfile(''hillArmControl.py'')');
py('eval', str);

nextTemp = py('get', 'nextState');
initialized = py('get', 'initialized');



next = [ nextTemp(2),nextTemp(1)];

end