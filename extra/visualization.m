load('C:\Users\Udaranga\OneDrive\EPFL - Masters\Semester 2\CS 498 Semester Project\PILCO\pilcoV0.9\scenarios\arm\run7_multipleRuns\pendulum_1_H80.mat');

thetaDot = -3:0.1:3;
theta = 0:0.1:pi/2;


for j = 1:length(theta)
    s = [0,theta(j),sin(theta(j)),cos(theta(j))];
    [xx, yy, realCost{j+J}, latent{j}] = ...
        rollout2([0 theta(j)], policy, HH, plant, cost);
    
end


%%
thetaDot = -3:0.1:3;
% theta = 0:0.1:pi/2;
theta = pi/4;
for iii = 1:length(thetaDot)
    for j = 1:length(theta)
        s = [thetaDot(iii),theta(j),sin(theta(j)),cos(theta(j))];
        cost.tstep = 1;
        L(iii,j) = cost.fcn(cost,s(dyno)',zeros(length(dyno)));;
        
    end
end

%%
clear all;
for iii = 1:19
    str = sprintf('..\\scenarios\\arm\\run7_multipleRuns\\pendulum_%d_H80.mat',iii);
    load(str);
%     disp(iii);
    policy_inputs(iii,:,:) = policy.p.inputs;
    policy_targets(iii,:,:) = policy.p.inputs;
    policy_hyp(iii,:,:) = policy.p.hyp;
    
   
    
    
%     size(x);
end
%%

for iii = 1:19
    plot(abs(asin(policy_inputs(iii,:,2))*180/pi)); 
%     axis([0 100 -2.5 2.5]);
%     axis([0 100 -1 3]);
%     axis([0 100 -3 4]);
    drawnow
    
    pause(0.5); 
end

%%


